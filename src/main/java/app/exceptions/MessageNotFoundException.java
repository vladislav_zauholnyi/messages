package app.exceptions;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MessageNotFoundException extends RuntimeException{
    private String message;
}
