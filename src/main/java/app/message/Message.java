package app.message;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@EqualsAndHashCode(of = "id")
@Setter
@Getter
@ToString(exclude = "id")
public class Message {
    private UUID id;
    private int firstValue;
    private int secondValue;
    private int sum;
}
