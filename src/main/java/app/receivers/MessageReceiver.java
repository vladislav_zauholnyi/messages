package app.receivers;

import app.message.Message;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static app.config.ActiveMQConfiguration.REPLY_CHANNEL;
import static app.config.ActiveMQConfiguration.REQUEST_CHANNEL;

@Log4j2
@Component
public class MessageReceiver {
    private final List<Message> messages = new ArrayList<>();
    private static final int EXPECTED_LOAD = 10;

    @Autowired
    private JmsTemplate jmsTemplate;

    @JmsListener(destination = REQUEST_CHANNEL)
    public void receiveMessage(@Payload Message incomingMessage) {
        log.info("RECEIVER. RECEIVED: " + incomingMessage);
        messages.add(incomingMessage);
        if (messages.size() == EXPECTED_LOAD) {
            for (Message message : messages) {
                message.setSum(message.getFirstValue() + message.getSecondValue());
                log.info("RECEIVER. REPLYING WITH: " + message);
                jmsTemplate.convertAndSend(REPLY_CHANNEL, message);
            }
            messages.clear();
        }
    }
}
