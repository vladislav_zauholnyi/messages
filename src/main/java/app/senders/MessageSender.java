package app.senders;

import app.exceptions.MessageNotFoundException;
import app.message.Message;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static app.config.ActiveMQConfiguration.REPLY_CHANNEL;
import static app.config.ActiveMQConfiguration.REQUEST_CHANNEL;

@Log4j2
@Service
public class MessageSender {
    private List<Message> messages;

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendMessages(int numberOfMessagesToSend) {
        messages = createMessages(numberOfMessagesToSend);
        for (Message message : messages) {
            log.info("SENDER. SENDING: " + message);
            jmsTemplate.convertAndSend(REQUEST_CHANNEL, message);
        }
    }

    @JmsListener(destination = REPLY_CHANNEL)
    public void receiveReply(@Payload Message message) {
        log.info("SENDER. RECEIVED: " + message);
        if (!messages.contains(message)) {
            throw new MessageNotFoundException("Reply Message ID doesn't match any of Request Messages IDs");
        }
    }

    private List<Message> createMessages(int numberToCreate) {
        List<Message> messages = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < numberToCreate; i++) {
            Message message = new Message();
            message.setId(UUID.randomUUID());
            message.setFirstValue(random.nextInt(99));
            message.setSecondValue(random.nextInt(99));
            messages.add(message);
        }
        return messages;
    }
}
